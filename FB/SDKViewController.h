//
//  SDKViewController.h
//  FB
//
//  Created by Daniel Phillips on 12/10/2012.
//  Copyright (c) 2012 Daniel Phillips. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface SDKViewController : UIViewController
- (IBAction)getMe:(id)sender;
- (IBAction)uploadVideo:(id)sender;

@end
